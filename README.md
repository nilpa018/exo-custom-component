# Exercice React : Création d'un composant réutilisable

## Objectif
Créer des composants réutilisables permettant de recevoir différents paramètres afin de changer rapidement leur apparence dans le rendu.

## Prérequis
Assurez-vous d'avoir Node.js installé sur votre machine.    

## Étapes

### Étape 1: Initialisation du projet React 

1. Clonez le repository depuis GitLab :
   ```bash
   git clone https://gitlab.com/nilpa018/exo-custom-component.git
   ```

2. Accédez au répertoire du projet :
   ```bash 
   cd exo-custom-component
   ```

3. Lancez l'installation des modules et construction du dossier public :
   ```bash
   npm install
   ```

4. Lancez l'application pour vous assurer que tout fonctionne correctement :
   ```bash
   npm run dev
   ```  

### Étape 2: Création du composant  

Ce composant doit accepter les propriétés suivantes :

**title**: Le texte du bouton.    
**link**: L'adresse du lien hypertexte ou d'une adresse e-mail.     
**arrDir**: La direction de la flèche, soit "left" ou "right".  
**radius**: La valeur optionnelle pour spécifier le nombre de pixels de radius des 4 angles du bouton.    


### Étape 3: Conditions de réussite
Les flèches (**faArrowLeft** et **faArrowRight**) proviendront de FontAwesome (https://fontawesome.com/docs/web/use-with/react/add-icons#add-individual-icons-explicitly).  
Le titre et la fleche du bouton doivent être centré horizontalement avec une apparence harmonieuse.     
Au clic, le bouton ouvre un nouvel onglet ou ouvre le client mail.  
Le composant est rangé dans un dossier components et dispose de sa propre feuille de style.     
Si le paramètre radius n'est pas transmis, le bouton aura les angles rectangulaires.   

Assurez-vous d'inclure le code source complet pour la vérification. 

### Étape 4: Vérification
Le candidat devra fournir l'ensemble des fichiers à l'exception des node_modules après avoir complété la tâche. 

### Étape 5: Styles (bonus)
Ajoutez des styles supplémentaires avec les fonctionnalités fournies par votre bibliothèque graphique si vous le souhaitez.

### Aperçu graphique
Voici une représentation de ce qui est attendu graphiquement, vous avez la possibilité de modifier un peu celui-ci.

![Representation graphique de l'exercice](/images/exo-custom-component.png)

### Remarques
- Afin de répondre aux meilleurs pratiques, déclarez vos variables de préférences avec const et let.  
- Vous avez le droit d'ajouter des fonctionnalités au projet (loader, tests, TypeScript etc...)
- Utilisez les composants de manière modulaire et réutilisable.   
- Commentez votre code pour expliquer votre démarche si c'est pertinent.

### Ressource
Documentation React : https://reactjs.org/docs/getting-started.html     
Documentation FontAwesome : https://fontawesome.com/docs/web/use-with/react/add-icons#add-individual-icons-explicitly   

### Soumission
Lorsque vous avez terminé, assurez-vous de fournir le code source du projet pour évaluation soit en l'envoyant au format ZIP sans y inclure les node_modules par [E-mail](mailto:nilpa018@yahoo.fr) soit en l'hébergeant sur un Drive afin de pouvoir télécharger celui-ci.